import math

def calculate(decimal):
    num = float(decimal)
    binary = []
    
    while num > 0:
        num = num / 2
        
        if num.is_integer():
            binary.append("0")
        else:
            binary.append("1")

        num = float(math.floor(num))

    binary.reverse()
    return ("".join(binary))
