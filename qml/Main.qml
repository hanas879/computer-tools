/*
 * Copyright (C) 2021  Henrik Enger Milo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * comtool.hem is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'comtool.hem.hanas879'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Python {
      id: python
      Component.onCompleted: {
        addImportPath(Qt.resolvedUrl("../src"));
        importModule_sync("calculate")
      }
      onError: {
        consol.log("python error: " + traceback)
      }
    }

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Computer Tools')

            Icon {
              id: icon
              height: parent.height
              width: parent.height
              name: "home"

              anchors {
                right: parent.right
              }
            }
        }

        TextField {
          id: textField
          placeholderText: "Enter decimal number: "
          height: units.gu(8)
          font.pixelSize: units.gu(3)

          anchors {
            top: header.bottom
            topMargin: units.gu(1)
            left: header.left
            leftMargin: units.gu(1)
            right: header.right
            rightMargin: units.gu(1)
          }
        }

        CalculatorButton {
          height: width / 2.3
          width: textField.width / 2.5
          color: "#28ff1d"
          color_text: "white"
          text: "Convert!"

          anchors {
            top: textField.bottom
            topMargin: units.gu(1)
            right: textField.right
          }

          onClicked: {
            python.call("calculate.calculate", [textField.text], function (result) {
              textArea.text = result;
            })
          }
        }

        Rectangle {
          id: result
          height: header.height
          width: header.width

          anchors {
            //top: parent.top
            //topMargin: parent.height / 2
            bottom: resultBox.top
          }

          Text {
            text: "Result: "
            font.family: header.font
            font.pixelSize: units.gu(2)
          }
        }

        Rectangle {
          id: resultBox
          width: textField.width
          height: parent.height / 4

          anchors {
            left: textField.left
            right: textField.right
            bottom: parent.bottom
            bottomMargin: units.gu(2)
          }

          TextArea {
            id: "textArea"
            anchors.fill: parent
            readOnly: true
            font.pixelSize: units.gu(3)
          }
        }
    }
}
