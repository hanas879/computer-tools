import QtQuick 2.7
import Ubuntu.Components 1.3

Rectangle {
  property alias text: label.text
  property alias color_text: label.color
  signal clicked

  color: "#28ff1d"
  radius: units.gu(2)
  border.width: units.gu(0.25)
  border.color: "white"

  Label {
    id: label
    font.pixelSize: units.gu(4)
    anchors.centerIn: parent
    color: "#111"
  }

  MouseArea {
    id: mouseArea
    anchors.fill: parent
    onClicked: parent.clicked()
  }
}
